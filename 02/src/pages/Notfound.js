import React from 'react';
import Logo from '../components/Logo';

const Notfound = () => {
    return (
        <div>
            <Logo/>
            <Navigation/>
        </div>
    );
};

export default Notfound;