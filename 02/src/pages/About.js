import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const About = () => {
    return (
        <div>
          <Logo/>  
          <Navigation/>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur totam similique nam qui reprehenderit autem provident, ipsa, explicabo at beatae dolorem ad nihil culpa nulla natus nesciunt accusantium nobis accusamus?</p>
        </div>
    );
};

export default About;