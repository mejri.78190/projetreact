import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//pour import scss
import './index.css';//import du index";



ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);



