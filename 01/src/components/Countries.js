import React, { useEffect, useState } from 'react';
import axios from 'axios';//faciliter le mappage, le parcours d'une base de donnée sous forme de fichier
import Card from './Card';



const Countries = () => {


    const [data, setData] = useState([]);
    const radios = [ "Africa","America", "Asia", "Europe", "Oceania" ];
    const [ selectedRadio, setSelectedRadio] = useState ('');
    const [ rangeValue, setRangeValue] = useState(50000000);//valeur par défault
    const [searchInput, setSearchInput] = useState("");



    useEffect( () =>{// useEffect = pour eviter que le code se répète
        axios.get("https://restcountries.com/v3.1/all")
        .then( (res)=>setData(res.data) );
    },[] ); //[] pour qu'elle se repète une fois
        //res= résultat
        //setter la variable personne
        //on fait console.log pour choisir la partie desirée en suivant le chemin ex data
        

        const numberFormat = (num) => {
            return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");//regular expression pour chercher un texte(search) ou le remplacer (replace)
          };//fonction pour les séparateurs de milliers
        





    return (

        <div className='countries'>
            
        <ul className='radio-container'>
        {

            radios.map(  (continent, index  ) => (

                <li key={index} >
                
                    <input type= "radio" name='continent' id={continent}
                    // e= objet en cours
                        onChange={ (e)  => setSelectedRadio( e.target.id ) }//onChange s'applique sur l'input de li (objet en cours), e.target= pour pointer sur l'input en cours
                    />
                    <label htmlFor = {continent} > {continent}</label> 
                    
                
                </li>

            )
            )

        }

        </ul>

        <input className='search'
        type= 'text'
        placeholder="entrez le nom d'un pays (en anglais)"//barre de recherche se vide dès qu'on écrit
        onChange={ (e) =>setSearchInput(e.target.value) }
        />


        <br/>
        <input 
            type='range'
            min= '0'//valeur min
            max= '1377482166' // valeur max
            defaultValue={rangeValue}
            onChange = { (e) => setRangeValue( e.target.value) } //numberFormat= mettre le séparateur de milliers sur le nombre fonction callback
        />
            
            <label id='range' > { numberFormat(rangeValue)}</label>
            
            
        <ul>
           
            {
             data
             .filter( (country) => country.region.includes( selectedRadio)  )
             .filter( ( country) => country.population > rangeValue )
             .filter( ( country) => country.name.common.toLowerCase().includes(searchInput.toLowerCase() ) )
             .map((country) =>  (<Card country={ country } pop={numberFormat(country.population) }  key = { country.name.common} /> ) //declarer les éléments dans Card
             //elmt=paramètre de chaque pays
            
             )} 
        
        </ul>

        </div>
    );
};

export default Countries;