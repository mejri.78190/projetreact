
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Post from './Post';




const Posts = () => {

    const [data, setData] = useState([]);
    const [searchTitle, setSearchTitle] = useState("");
    const [searchAuteur, setSearchAuteur] = useState("");




    useEffect(() => {
        axios.get("https://jsonplaceholder.typicode.com/posts")
            .then((res) => setData(res.data));


    }, []);


    return (

        
        <div className='posts'>


        <input className='search' type= "text"
          placeholder="entrez un titre" 
          onChange={ (e) =>setSearchTitle(e.target.value) }
        />
        <br/>
        <input className='search' type= "number"//avec un scrolling pour les nombres
            min="0" max="9"

          placeholder="entrez votre numero" 
          onChange={ (e) =>setSearchAuteur(e.target.value) }
        />


            <ul>

                {
                    data
                    .filter((post)=> post.title.toLowerCase().includes(searchTitle.toLowerCase()))
                    .filter((post)=> post.userId.toString().includes (searchAuteur))
                    .map((post) => (<Post post={ post} key= { post.id}/>)
                    )}
            </ul>

        </div>



            


           
        

    
    );
};


export default Posts;