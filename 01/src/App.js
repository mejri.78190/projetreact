import { BrowserRouter, Routes, Route } from 'react-router-dom';
import About from './pages/About';
import Connexion from './pages/Connexion';
import Contact from './pages/Contact';
import Home from './pages/Home';
import Test from './pages/Test';
import Notfound from './pages/Notfound';
import Blog from './pages/Blog';



const App = () => {
  return (
   //pour trouver les différentes pages sur le navigateur
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/about' element={<About />} />
        <Route path='/contact' element={<Contact/>}/>
        <Route path='/connexion' element={<Connexion/>}/>
        <Route path='/test' element={ <Test/>}/>
        <Route path='/blog' element={<Blog/>} />
        <Route path='*' element={<Notfound />} />


      </Routes>

    </BrowserRouter>

  );
};

export default App;