import React, { Component } from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

class About extends Component {
    render() {
        return (
            <div>
                <Logo />
                <Navigation />
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab, necessitatibus dolorum saepe odio alias assumenda laborum culpa autem? Placeat perspiciatis corrupti quibusdam vero odit animi dolor reprehenderit totam sunt impedit.</p>
            </div>
        );
    }
}

export default About;