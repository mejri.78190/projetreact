import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const Contact = () => {

const names =[ 'James', 'John', 'Paul', 'Rambo', 'George'];//tableau

const people = [//ce sont des objets les 5 parties

{
    nom :"James",
    age : 31,
//objet= person
},
{
    nom :"John",
    age : 45,

},
{
    nom :"Paul",
    age : 65,

},
{
    nom :"Rambo",
    age : 131,

},
{
    nom :"Georges",
    age : 75,

}

];

    return (
        <div>
        <Logo/>
         <Navigation/>
            <h2> Tableau filtré</h2>
         <ul>
        {
            names
            .filter((name) => name.includes ('J'))//filtrer pour avoir les noms commencant par J
            .map(
                (filterName) => (
                    <li>
                        {filterName}
                    </li>

                )
            )
        }

         </ul>
                <h2>Objet filtré</h2>
        <ul>
        {
            people
            .filter( (person) => person.age > 60)//flitrer par rapport à l'age

            .map (
                (filtredPerson) => (<li> {filtredPerson.nom}</li>) //parcourir le tableau
            )

        }

        </ul>







        </div>
    );
};

export default Contact;
