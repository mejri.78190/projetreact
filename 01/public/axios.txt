import React, { useEffect, useState } from 'react';
import axios from 'axios';//faciliter le mappage, le parcours d'une base de donnée sous forme de fichier



const Countries = () => {


    const [personnes, setPersonnes] = useState([]);

    useEffect( () =>{// useEffect = pour eviter que le code se répète
        axios.get("https://jsonplaceholder.typicode.com/users")
        .then( (res)=>setPersonnes(res.data) );
    },[] ); //[] pour qu'elle se repète une fois
        //res= résultat
        //setter la variable personne
        //on fait console.log pour choisir la partie desirée ex data
        
    return (
        
        <ul>
           
            {
             personnes.map( ( personne) => <li>{personne.name}</li> ) 
            
            } 
        
        </ul>
    );
};

export default Countries;